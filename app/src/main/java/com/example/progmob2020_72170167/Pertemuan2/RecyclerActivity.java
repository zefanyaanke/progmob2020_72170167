package com.example.progmob2020_72170167.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.progmob2020_72170167.Adapter.MahasiswaRecyclerAdapter;
import com.example.progmob2020_72170167.Model.Mahasiswa;
import com.example.progmob2020_72170167.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        Mahasiswa m1 = new Mahasiswa("Zefanya", "72170167", "0812345678");
        Mahasiswa m2 = new Mahasiswa("Laura", "72170168", "0812345679");
        Mahasiswa m3 = new Mahasiswa("Berta", "72170169", "0812345680");
        Mahasiswa m4 = new Mahasiswa("Nana", "72170170", "0812345678");
        Mahasiswa m5 = new Mahasiswa("Nini", "72170171", "0812345678");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);
    }
}